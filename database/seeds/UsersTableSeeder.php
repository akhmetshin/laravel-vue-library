<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email'    => 'admin@test.com',
            'password' => Hash::make('testadmin'),
            'role'     => 2
        ]);

        User::create([
            'email'    => 'employee@test.com',
            'password' => Hash::make('testemployee'),
            'role'     => 1
        ]);

        User::create([
            'email'    => 'user@test.com',
            'password' => Hash::make('testuser'),
            'role'     => 0
        ]);
    }
}
