<?php

use App\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::create([
            'name' => 'Атлант расправил плечи',
            'author' => 'Айн Рэнд',
            'genre' => 'Классическая и современная проза',
            'publisher' => 'Альпина Паблишер'
        ]);

        Book::create([
            'name' => '1984',
            'author' => 'Джордж Оруэлл',
            'genre' => 'Классическая и современная проза',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'Щегол',
            'author' => 'Донна Тартт',
            'genre' => 'Детектив',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'Отель',
            'author' => 'Артур Хейли',
            'genre' => 'Детектив',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'Метро 2033',
            'author' => 'Дмитрий Глуховский',
            'genre' => 'Фантастика',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'Оно',
            'author' => 'Стивен Кинг',
            'genre' => 'Фантастика',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'После ссоры',
            'author' => 'Анна Тодд',
            'genre' => 'Романтика',
            'publisher' => 'Издательство Э'
        ]);

        Book::create([
            'name' => 'Одно небо на двоих',
            'author' => 'Шерри Аргов',
            'genre' => 'Романтика',
            'publisher' => 'Эксмо'
        ]);

        Book::create([
            'name' => 'Гарри Поттер и философский камень',
            'author' => 'Джоан Роулинг',
            'genre' => 'Книги для детей',
            'publisher' => 'Махаон'
        ]);

        Book::create([
            'name' => 'Дом у Змеиного озера',
            'author' => 'Евгений Гаглоев',
            'genre' => 'Книги для детей',
            'publisher' => 'Росмэн'
        ]);

        Book::create([
            'name' => 'Sapiens. Краткая история человечества',
            'author' => 'Юваль Ной Харари',
            'genre' => 'Наука и техника',
            'publisher' => 'Синдбад, Издательство, ООО'
        ]);

        Book::create([
            'name' => 'Искусство цвета',
            'author' => 'Иоганнес Иттен',
            'genre' => 'Наука и техника',
            'publisher' => 'Аронов'
        ]);

        Book::create([
            'name' => 'Краткая история США',
            'author' => 'Роберт Римини',
            'genre' => 'Общество',
            'publisher' => 'КоЛибри'
        ]);

        Book::create([
            'name' => 'Искусство войны',
            'author' => 'Сунь-Цзы',
            'genre' => 'Общество',
            'publisher' => 'АСТ'
        ]);

        Book::create([
            'name' => 'Курс элементарной кулинарии',
            'author' => 'Курс элементарной кулинарии',
            'genre' => 'Увлечения',
            'publisher' => 'КоЛибри'
        ]);

        Book::create([
            'name' => 'Шахматы. Начальная тактика',
            'author' => 'Анна Дорофеева',
            'genre' => 'Увлечения',
            'publisher' => 'Дорофеева Анна Геннадьевна'
        ]);
    }
}
