import VueRouter from 'vue-router'
import Home from './pages/Home'
import Register from './pages/auth/Register'
import Login from './pages/auth/Login'
import ForgotPassword from './pages/auth/ForgotPassword'
import ResetPassword from './pages/auth/ResetPassword'
import Book from "./pages/Book"
import UserBookings from "./pages/UserBookings"
import AdminBookList from './pages/admin/book/BookList'
import AdminBookCreate from './pages/admin/book/BookCreate'
import AdminBookEdit from './pages/admin/book/BookEdit'
import AdminUserList from './pages/admin/user/UserList'
import AdminUserCreate from './pages/admin/user/UserCreate'
import AdminUserEdit from './pages/admin/user/UserEdit'
import NotFound from './components/404'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            title: 'Главная страница',
            auth: undefined
        }
    },
    {
        path: '/book/:id',
        name: 'showBook',
        component: Book,
        meta: {
            auth: undefined,
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            title: 'Регистрация',
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            title: 'Логин',
            auth: false
        }
    },
    {
        path: '/forgot',
        name: 'forgotPassword',
        component: ForgotPassword,
        meta: {
            title: 'Сбросить пароль',
            auth: false
        }
    },
    {
        path: '/reset-password',
        name: 'resetPassword',
        component: ResetPassword,
        meta: {
            title: 'Сбросить пароль',
            auth: false
        }
    },
    {
        path: '/bookings',
        name: 'userBookings',
        component: UserBookings,
        meta: {
            title: 'Мои бронирования',
            auth: true
        }
    },
    {
        path: '/admin/books',
        name: 'admin.books',
        component: AdminBookList,
        meta: {
            title: 'Список книг',
            auth: { roles: [1, 2], redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '/admin/books/create',
        name: 'admin.bookCreate',
        component: AdminBookCreate,
        meta: {
            title: 'Создать книгу',
            auth: { roles: [1, 2], redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '/admin/books/:id',
        name: 'admin.bookEdit',
        component: AdminBookEdit,
        meta: {
            title: 'Редактировать книгу',
            auth: { roles: [1, 2], redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '/admin/users',
        name: 'admin.users',
        component: AdminUserList,
        meta: {
            title: 'Список пользователей',
            auth: { roles: 2, redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '/admin/users/create',
        name: 'admin.userCreate',
        component: AdminUserCreate,
        meta: {
            title: 'Создать пользователя',
            auth: { roles: 2, redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '/admin/users/:id',
        name: 'admin.userEdit',
        component: AdminUserEdit,
        meta: {
            title: 'Редактировать пользователя',
            auth: { roles: 2, redirect: { name: 'login' }, forbiddenRedirect: '/403' }
        }
    },
    {
        path: '*',
        component: NotFound
    }
];

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});

export default router