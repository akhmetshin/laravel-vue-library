## Библиотека

Laravel + VueJS

Доступные роли: администратор, библиотекарь и клиент (пользователь).

* Регистрация, авторизация, восстановление пароля по E-mail
* Управление пользователями
* Управление книгами
* Выдача и получение книг от клиентов
* Просмотр списка книг, поиск
* Возможность создать и отменить бронирование на книгу
* Возможность создать напоминание по E-mail, будет отправлено письмо о доступности книги (не забронирована, в наличии)
* Возможность оценивать книги
* Возможность оставлять комментарии

### Установка

- `git clone https://bitbucket.org/akhmetshin/laravel-vue-library.git`
- `cd laravel-vue-library`
- `composer install`
- `npm install`
- `cp .env.example .env`

В файле .env необходимо добавить/изменить значения:
```
APP_NAME=Библиотека
APP_URL=http://127.0.0.1:8000
BOOKING_TIME=60 # Время бронирования

DB_DATABASE=<база данных>
DB_USERNAME=<пользователь>
DB_PASSWORD=<пароль>

# Почтовый ящик для отправки писем
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=Библиотека
```

- `php artisan key:generate`
- `php artisan jwt:secret`
- `php artisan migrate --seed`
- `npm run dev`
- `php artisan serve`
- http://127.0.0.1:8000

Тестовые пользователи:

- логин: `admin@test.com`, пароль: `testadmin`
- логин: `employee@test.com`, пароль: `testemployee`
- логин: `user@test.com`, пароль: `testuser`

Для выполнения очистки истекших бронирований и отправки уведомлений необходимо настроить планировщик (https://laravel.com/docs/5.8/scheduling#introduction) или запустить его вручную `php artisan schedule:run`.
