<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('reset-password', 'Auth\ResetPasswordController@reset');
    Route::get('refresh', 'Auth\TokenRefreshController@refresh');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('user', 'Auth\ReturnUserController@user');
        Route::post('logout', 'Auth\LoginController@logout');
    });
});

Route::prefix('book')->group(function () {
    Route::get('getAll', 'BookController@getBooks');
    Route::get('getByKeyword', 'BookController@findBooksByKeyword');
    Route::get('{book}/getBook', 'BookController@getBookById');
    Route::get('{book}/getRating', 'Book\VoteController@getRating');
    Route::get('{book}/getComments', 'Book\CommentController@getCommentsByBookId');
});

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('mybookings', 'Book\BookingController@getAllByUserId')->middleware('userBinding');

    Route::group(['prefix' => 'book', 'middleware' => 'userBinding'], function () {
        Route::post('{book}/setVote', 'Book\VoteController@setVote');
        Route::post('{book}/booking', 'Book\BookingController@booking');
        Route::put('{book}/booking', 'Book\BookingController@cancel');
        Route::post('{book}/subscribe', 'Book\SubscriptionController@subscribe');
        Route::post('{book}/sendComment', 'Book\CommentController@createComment');
    });

    Route::prefix('admin')->group(function () {
        Route::get('users', 'UserController@index');
        Route::post('users', 'UserController@store');
        Route::get('users/{user}', 'UserController@show');
        Route::put('users/{user}', 'UserController@update');
        Route::delete('users/{user}', 'UserController@destroy');

        Route::group(['middleware' => 'isAdminOrLibrarian'], function() {
            Route::get('books', 'BookController@getBooksWithBookings');
            Route::post('books', 'BookController@store');
            Route::get('books/{book}', 'BookController@show');
            Route::put('books/{book}', 'BookController@update');
            Route::delete('books/{book}', 'BookController@destroy');

            Route::put('/books/{book}/give', 'BookController@giveBook');
            Route::put('/books/{book}/receive', 'BookController@receiveBook');
        });
    });
});

Route::fallback(function() {
    return response()->json(['error' => 'Not Found.'], 404);
});
