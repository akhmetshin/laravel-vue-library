<?php

namespace App\Console\Commands;

use App\Notifications\BookAvailable;
use App\Subscription;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SendUserBookIsAvailable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:book-available';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = Subscription::with(['book', 'user'])->whereHas('book', function (Builder $query) {
            $query->where('isAvailable', '=', true);
        })->get();

        foreach ($subscriptions as $subscription) {
            $subscription->user->notify(new BookAvailable($subscription->book));
            $subscription->delete();
        }
    }
}
