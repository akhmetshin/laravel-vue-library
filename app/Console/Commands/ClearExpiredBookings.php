<?php

namespace App\Console\Commands;

use App\Book;
use App\Booking;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearExpiredBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear-expired-bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiredBookings = Booking::where('expires', '<', Carbon::now())
            ->get()
            ->toArray();

        $bookingIds = array_map(static function ($booking) {
            return $booking['id'];
        }, $expiredBookings);

        $bookIds = array_map(static function ($book) {
            return $book['book_id'];
        }, $expiredBookings);

        Booking::whereIn('id', $bookingIds)->delete();
        Book::whereIn('id', $bookIds)->update(['isAvailable' => true]);
    }
}
