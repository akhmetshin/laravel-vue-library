<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'book_id', 'user_id', 'value'
    ];

    public $timestamps = false;
}
