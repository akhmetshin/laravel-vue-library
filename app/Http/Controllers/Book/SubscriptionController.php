<?php

namespace App\Http\Controllers\Book;

use App\Book;
use App\Booking;
use App\Subscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    /**
     * Создаем напоминание на книгу
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function subscribe(Request $request, Book $book): JsonResponse
    {
        if ($book->isAvailable && Booking::where('book_id', $book->id)->where('user_id', $request->currentUser->id)->exists()) {
            return response()->json(['error' => 'Книга есть в наличии, нельзя создать напоминание.'], 400);
        }

        Subscription::firstOrCreate(['book_id' => $book->id, 'user_id' => $request->currentUser->id]);

        return response()->json(['success' => true]);
    }
}
