<?php

namespace App\Http\Controllers\Book;

use App\Book;
use App\Booking;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class BookingController extends Controller
{
    /**
     * Получить список всех бронирований пользователя
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllByUserId(Request $request): JsonResponse
    {
        $bookings = Booking::with('book')
            ->where('user_id', $request->currentUser->id)
            ->get();

        return response()->json($bookings);
    }

    /**
     * Бронирование книги. Возвращает ошибку: если книги нет в наличии или
     * по данной книге уже создано бронирование (проверка по уникальному полю book_id).
     *
     * При создании бронирования дополнительно удаляем подписку на книгу (если подписка существует).
     * Это необходимо для решения ситуации, при которой книга стала доступна для бронирования,
     * но при этом подписка еще не была обработана (действия были совершены между срабатываниями планировщика).
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function booking(Request $request, Book $book): JsonResponse
    {
        try {
            $response = [];
            $code     = 200;

            if (Book::where('id', $book->id)->where('isAvailable', true)->update(['isAvailable' => false])) {
                Subscription::where('book_id', $book->id)->where('user_id', $request->currentUser->id)->delete();

                Booking::create([
                    'book_id' => $book->id,
                    'user_id' => $request->currentUser->id,
                    'expires' => Carbon::now()->addMinutes(Config::get('booking.booking_time'))
                ]);

                $response['success'] = true;
            } else {
                $response['error'] = 'Не удалось создать бронирование. Книга не найдена или нет в наличии.';
                $code = 400;
            }
        } catch (QueryException $e) {
            $response['error'] = 'Не удалось забронировать книгу';
            $code = 400;
        } finally {
            return response()->json($response, $code);
        }
    }

    /**
     * Отмена бронирования
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function cancel(Request $request, Book $book): JsonResponse
    {
        $response = [];
        $code     = 200;

        if (Booking::where('book_id', $book->id)->where('user_id', $request->currentUser->id)->delete()) {
            $book->update([
                'isAvailable' => true
            ]);

            $response['success'] = true;
        } else {
            $response['error'] = 'Бронирование не найдено или не принадлежит текущему пользователю';
            $code = 403;
        }

        return response()->json($response, $code);
    }
}
