<?php

namespace App\Http\Controllers\Book;

use App\Book;
use App\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Создать новый комментарий
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function createComment(Request $request, Book $book): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        Comment::create([
            'book_id' => $book->id,
            'user_id' => $request->currentUser->id,
            'text'    => $request->text
        ]);

        return response()->json(['success' => true]);
    }

    /**
     * Получить все комментарии для книги
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function getCommentsByBookId(Book $book): JsonResponse
    {
        $comments = Comment::with('user')
            ->where('book_id', $book->id)
            ->orderByDesc('created_at')
            ->get();

        return response()->json($comments);
    }
}
