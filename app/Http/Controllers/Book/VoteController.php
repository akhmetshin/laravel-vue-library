<?php

namespace App\Http\Controllers\Book;

use App\Book;
use App\Vote;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class VoteController extends Controller
{
    /**
     * Установить оценку книги
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function setVote(Request $request, Book $book): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required|integer|between:1,5'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        Vote::updateOrCreate(
            ['book_id' => $book->id, 'user_id' => $request->currentUser->id],
            ['value' => $request->value]
        );

        return response()->json(['success' => true]);
    }

    /**
     * Получить рейтинг книги и пред. оценку (если пользователь авторизован)
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function getRating(Book $book): JsonResponse
    {
        $rating = Vote::where('book_id', $book->id)->avg('value');

        $response = [
            'value'         => $rating ? (float)$rating : 0,
            'previous_vote' => null
        ];

        if (($user = JWTAuth::user()) && $userVote = Vote::where('user_id', $user->id)->where('book_id', $book->id)->first()) {
            $response['previous_vote'] = $userVote->value;
        }

        return response()->json($response);
    }
}
