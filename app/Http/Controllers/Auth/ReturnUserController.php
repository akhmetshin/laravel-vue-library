<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReturnUserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request): JsonResponse
    {
        $user = User::find(Auth::user()->id);

        return response()->json([
            'status' => 'success',
            'data'   => $user
        ]);
    }
}
