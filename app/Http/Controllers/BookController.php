<?php

namespace App\Http\Controllers;

use App\Book;
use App\Booking;
use App\Subscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class BookController extends Controller
{
    /**
     * Получить список всех книг (главная страница)
     *
     * @return JsonResponse
     */
    public function getBooks(): JsonResponse
    {
        $books = Book::all();

        return response()->json($books);
    }

    /**
     * Получить книги используя ключевое слово (поиск)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function findBooksByKeyword(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'keyword'=> 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->all()
            ], 422);
        }

        $keyword = '%' . $request->keyword . '%';

        $books = Book::where('name', 'LIKE', $keyword)
            ->orWhere('author', 'LIKE', $keyword)
            ->orWhere('genre', 'LIKE', $keyword)
            ->orWhere('publisher', 'LIKE', $keyword)
            ->get();

        return response()->json($books);
    }

    /**
     * Проверяем был ли пользователь уже подписан
     *
     * @param int $bookId
     * @param int $userId
     * @return bool
     */
    private function isSubscriptionExists(int $bookId, int $userId): bool
    {
        return Subscription::where('book_id', $bookId)
            ->where('user_id', $userId)
            ->exists();
    }

    /**
     * Получить бронирование
     *
     * @param int $bookId
     * @return Booking|null
     */
    private function getBooking(int $bookId): ?Booking
    {
        return Booking::where('book_id', $bookId)->first();
    }

    /**
     * Получить книгу по ID
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function getBookById(Book $book): JsonResponse
    {
        $response = [
            'book'           => $book,
            'allowBooking'   => false,
            'allowSubscribe' => false
        ];

        // Если пользователь не авторизован то возвращаем только книгу, бронирование и подписка запрещены
        if (!$user = JWTAuth::user()) {
            return response()->json($response);
        }

        // Проверяем, подписан ли текущий пользователь на книгу
        $subscriptionIsExists = $this->isSubscriptionExists($book->id, $user->id);

        if ($booking = $this->getBooking($book->id)) {
            if (($booking->user->id !== $user->id) && !$subscriptionIsExists) {
                $response['allowSubscribe'] = true;
            }
        } else if ($book->isAvailable) {
            $response['allowBooking'] = true;
        } else if (!$subscriptionIsExists) {
            $response['allowSubscribe'] = true;
        }

        return response()->json($response);
    }

    /**
     * Получить список всех книг с бронированиями (для библиотекаря)
     *
     * @return JsonResponse
     */
    public function getBooksWithBookings(): JsonResponse
    {
        $books = Book::with(['booking.user'])->get();

        return response()->json($books);
    }

    /**
     * Создать книгу
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required|string|max:255',
            'author'      => 'nullable|max:255',
            'genre'       => 'nullable|max:255',
            'publisher'   => 'nullable|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->all()
            ], 422);
        }

        $book = Book::create($request->all());

        return response()->json($book);
    }

    /**
     * Получить книгу
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function show(Book $book): JsonResponse
    {
        return response()->json($book);
    }

    /**
     * Обновить книгу
     *
     * @param Request $request
     * @param Book $book
     * @return JsonResponse
     */
    public function update(Request $request, Book $book): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required|string|max:255',
            'author'      => 'nullable|max:255',
            'genre'       => 'nullable|max:255',
            'publisher'   => 'nullable|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->all()
            ], 422);
        }

        $book->update($request->all());

        return response()->json($book);
    }

    /**
     * Удалить книгу
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function destroy(Book $book): JsonResponse
    {
        try {
            $book->delete();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Не удалось удалить книгу'], 500);
        }
    }

    /**
     * Выдать книгу на руки и отменить бронирование (для библиотекаря)
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function giveBook(Book $book): JsonResponse
    {
        Booking::where('book_id', $book->id)
            ->delete();

        $book->update(['isAvailable' => false]);

        return response()->json(['success' => true]);
    }

    /**
     * Вернуть книгу (для библиотекаря)
     *
     * @param Book $book
     * @return JsonResponse
     */
    public function receiveBook(Book $book): JsonResponse
    {
        $book->update(['isAvailable' => true]);

        return response()->json(['success' => true]);
    }
}
