<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\UserNotDefinedException;

class UserBinding
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $currentUser = auth()->userOrFail();
            $request->request->add(['currentUser' => $currentUser]);

            return $next($request);
        } catch (UserNotDefinedException $e) {
            return response()->json('Пользователь не найден', 404);
        }
    }
}
