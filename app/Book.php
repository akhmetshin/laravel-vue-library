<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'id', 'name', 'author', 'genre', 'publisher', 'isAvailable'
    ];

    public function setIsAvailableAttribute($isAvailable)
    {
        $this->attributes['isAvailable'] = filter_var($isAvailable, FILTER_VALIDATE_BOOLEAN);
    }

    public function booking()
    {
        return $this->hasOne('App\Booking');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\User');
    }
}
